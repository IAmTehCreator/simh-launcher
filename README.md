# SIMH Launcher
This repository contains a launcher for the [SIMH](http://simh.trailing-edge.com/) set of minicomputer emulators written in BASH script. The launcher helps manage the SIMH workspace and configuration for the emulators and can be installed so that it is available regardless of where you are in the terminal.

There is no guarantee that this will work on anything other than Mac OS X! I wrote this script solely to help me play about with the [PDP-8](https://en.wikipedia.org/wiki/PDP-8), [IBM 1130](https://en.wikipedia.org/wiki/IBM_1130) and the [Altair 8800](https://en.wikipedia.org/wiki/Altair_8800) as I found SIMH a bit daunting at first. I never intended for anybody else to use it, but if it helps you play with your virtual PDP-8 then great :)

I recommend using SIMH (with this launcher of course) on [Cathode](http://www.secretgeometry.com/apps/cathode/) an awesome terminal for OSX which emulates old CRT teletypes with a handful of themes, fonts and effects. Everything you need to maximise your immersion!

This repository does not contain any of the SIMH emulators, they will need to be downloaded from [simh.trailing-edge.com](http://simh.trailing-edge.com/) and compiled. The launcher can automatically compile SIMH from the zip file download from the website using the `simh install simh from <file>` command so long as you have moved the zip file into the launcher directory. The launcher is also able to automatically download version 39 of SIMH if you don't want to visit the website directly, but you are strongly advised to visit the website yourself, don't be lazy! ;)

## Installation
Once the SIMH emulators have been put into the `emulators/` directory you can run the launcher by going to it's directory and running the command `./launcher`. You will be prompted to specify the location of the launcher directory, this is used when the launcher is installed. The launcher directory is likely the one that you are already in which is shown to you as the "Current Directory", you can copy and paste that as the launcher directory.

Once the launcher directory is setup you can install the launcher to `/usr/local/bin` so that it is available wherever you are in the terminal. The launcher can be installed with the `./launcher install` command. Once the launcher is installed, it can be invoked at any time in the terminal with the `simh` command.

## Usage
The launcher adds various commands for dealing with the virtual machines and their configurations. The launcher maintains a 'workspace' which contains a collection of emulated machines that have been configured, when freshly installed this workspace is empty. When you have machines configured in it the launcher allows you to specify commands which will create configurations for the machines or run existing configurations. You can see a list of available commands with the `simh help` command.

### Updating
The SIMH launcher can update itself with the `simh update` command which will connect to this repository and download the latest tag and extract the updated files. If you have created a shortcut for the launcher using the `simh install` command then the shortcut will also be updated. You can manually update the launcher by downloading the desired version and extracting it into the launcher directory. If you have a shortcut installed as well then you will need to update it too by typing the command `./launcher install` after you have unpacked the new version.

### Example Session
Below is an example session with the launcher which compiles SIMH and brings a PDP-8 onto the workspace then configures it to run OS-8 from the RX01 floppy disk drive.

```
$ ./launcher install
SIMH directory not specified! The directory is likely the one that you
are currently in now if you have run this with the command ./launcher, if
this is the case then copy the 'Current Directory' and paste it as the
launcher directory. If you are not running this from the launcher directory
then type the correct directory path;

Current Directory: /Users/stevenmilne/Projects/simh-launcher
Launcher Directory: /Users/stevenmilne/Projects/simh-launcher

This will install this script into the /usr/local/bin directory so that
it is available regardless of which directory you are in.

Install as /usr/local/bin/simh (y/n): y
Installing...

SIMH launcher has been installed as /usr/local/bin/simh, you
can now type 'simh' anywhere at the command prompt and it will
launch. You can uninstall it with the 'uninstall' option.

$ simh install simh from simhv39-0
Compile SIMH from simhv39-0.zip? (y/n): y
Extracting SIMH from simhv39-0.zip...
Compiling SIMH...
Installing emulators...

$ simh workspace available

Available Machines to Add:
altair          h316            i7094           lgp             pdp11           pdp8            swtp6800mp-a
altairz80       hp2100          ibm1130         nova            pdp15           pdp9            swtp6800mp-a2
eclipse         i1401           id16            pdp1            pdp4            s3              vax
gri             i1620           id32            pdp10           pdp7            sds             vax780

$ simh workspace add pdp8
Adding pdp8 to workspace...

$ simh pdp8 shortcut
This will install a shortcut that will allow the machine to be invoked by
typing the pdp8 at the BASH prompt. Do you want to continue? (y/n): y
Installing shortcut to pdp8...

$ pdp8 set os8
```

The above session brings a PDP-8 emulator into the workspace which created the directory `simh/pdp8` (assuming `simh` is the directory set at the top of the launcher script) containing the PDP-8 emulator and a bunch of template directories. A shortcut to the PDP-8 emulator was also installed so typing `pdp8` anywhere in the BASH prompt would invoke the emulator. The final `simh set os8` command would bring up the [nano](https://www.nano-editor.org/) text editor where the configuration for the PDP-8 could be entered, below is the configuration entered;

```
; Setup the CPU and core memory
set cpu 32k
set cpu idle

; Attach installation RX01 floppy disks
attach rx0 disk/os8.rx01
attach rx1 disk/os8f4.rx01

; Clear the screen and start the emulator
! clear
boot rx0
```

The configuration should be in the do file format that SIMH accepts as the emulator will be invoked with this file to setup the instance. This configuration sets the emulated PDP-8 to have 32 KWords of core memory and have two RX01 floppy disks located in the `simh/pdp8/disk/` directory. The session continues to run the emulator;

```
$ tree pdp8
pdp8
├── card
├── cfg
│   └── os8
├── disk
│   ├── os8.rx01
│   └── os8f4.rx01
├── documents
├── extra
├── pdp8
└── tape

$ simh extensions
Extensions:
files
ibm1130
pdp8-wps

$ pdp8 install files
Installing extension files on machine pdp8...

$ pdp8 disks

Disks:
os8.rx01        os8f4.rx01

$ pdp8 os8

.DIR


SYS  VOLUME--   1
SYS:=RX8E
OS/8 SYSTEM   VERSION   3Q

BUILD .SV  33           HELP  .SV   8           BASIC .UF   4
ABSLDR.SV   5           PAL8  .SV  19           BCOMP .SV  17
BITMAP.SV   5           PIP   .SV  11           BLOAD .SV   8
BOOT  .SV   5           PT8E  .BN   1           BRTS  .SV  15
CCL   .SV  18           RESORC.SV  10           EABRTS.BN  24
CREF  .SV  13           RXCOPY.SV   6           RESEQ .BA   6
DIRECT.SV   7           SABR  .SV  24           ECHO  .SV   2
EDIT  .SV  10           TECO  .SV  22           RKLFMT.SV   9
EPIC  .SV  14           BASIC .AF   4           SET   .SV  14
FBOOT .SV   2           BASIC .FF   4           BATCH .SV  10
FOTP  .SV   8           BASIC .SF   4           FUTIL .SV  26
HELP  .HL  55           BASIC .SV   9           IDS   .SV   5

  36 FILES IN  437 BLOCKS -    1 FREE BLOCKS

.^E
Simulation stopped, PC: 01207 (KSF)
sim> q
Goodbye
RX: writing buffer to file

$ simh archive pdp8
This will compress and add the machine pdp8 to the archive, continue? (y/n): y
Remove pdp8 from the workspace? (y/n): n
Archiving pdp8...

$ tree
simh
├── archive
│   └── pdp8.tar.gz
├── emulators
│   ├── pdp8
│   └── ... (Available emulators)
├── extensions
│   ├── files
│   ├── files_cmd
│   ├── ibm1130
│   ├── ibm1130_cmd
│   ├── pdp8-wps
│   └── pdp8-wps_cmd
├── pdp8
│   ├── cfg
│   │   └── os8
│   ├── disk
│   │   ├── os8.rx01
│   │   └── os8f4.rx01
│   ├── documents
│   ├── extension
│   ├── extension_cmd
│   ├── extra
│   ├── pdp8
│   └── tape
├── src
│   └── ... (SIMH source code)
├── README.md
├── launcher
└── VERSION
```

### Commands
Below is a list of all the commands that the launcher accepts. Parts of the command enclosed within `<` and `>` must be replaced with the name of a machine or configuration. The `...` specifies that a further command must be specified, where-as `[...]` specifies that optional commands may be specified to modify the operation.

```
simh install [...]           -  Installs this launcher so it is always available
  from <file>                -  Installs the SIMH emulators from the given zip file"
simh uninstall               -  Uninstalls this launcher (does not remove SIMH)
simh machines                -  Shows a list of machines that have been configured
simh help                    -  Shows this help screen
simh connect                 -  Connects to a machine using telnet port 4000, to exit
	                            type ^] to enter telnet command mode and type quit
simh update                  -  Updates the launcher to the latest version
simh download emulators      -  Downloads the SIMH package of emulators and compiles
                                them for you. It is recommended to visit the host website
                                instead at http://simh.trailing-edge.com/
simh <machine> ...
  help                       -  Shows all commands available for the machine
  list                       -  Lists all of the available configurations for the machine
  show actions               -  Lists all of the script actions for the machine
  show action <action>       -  Shows the source of the specified script action
  show <configuration>       -  Shows a configuration file
  set action <action>        -  Creates or edits the specified script action
  set <configuration> [...]  -  Edits a configuration file
    note                     -  Edits a configuration note file
  copy <cfg> as <cfg>        -  Copies a configuration
  <configuration>            -  Runs the emulator with the given configuration file
  install <extension>        -  Installs a launcher extension for this machine
  uninstall                  -  Removes the launcher extension for this machine
  shortcut                   -  Installs a shortcut to this machine so it is always available
simh workspace ...
  available                  -  Shows all available machines that can be configured
  add <machine> [...]        -  Installs a machine into the workspace
    from <machine>           -  Copies an existing machine configuration
  remove <machine>           -  Deletes a machine configuration from the workspace
simh archive ...
  <machine> [...]            -  Archives a machine from the workspace
    as <machine>             -  Archives a machine under a different name
  restore <machine> [...]    -  Restores a machine from the archive to the workspace
    as <machine>             -  Restores a machine with a different name
  remove <machine>           -  Deletes an archived machine
simh extensions              -  Shows a list of launcher extensions to be installed
```

## Extensions
The launcher supports extension scripts which can be installed on individual emulators, when the user types a command which the launcher doesn't understand then the installed extension on the current machine will be given a chance to handle it. You can write your own extensions as BASH scripts, the extensions are provided a with some arguments `$1` is the name of the machine and `$2` to `$5` are the extra arguments supplied by the user.

You can put extensions within the `extensions/` directory so that the launcher can install them in machine configurations. Some extensions have been pre-packaged with the launcher to demonstrate the kinds of things that can be done by them;

* files  -  A generic extension that can be installed on any emulator
* ibm1130  -  An extension for the IBM 1130 emulator which manages `.job` and `.deck` files
* pdp8-wps  -  An extension for the PDP-8 which runs xterm so that the terminal can understand the output of the emulator

An extension must have a `_cmd` prefixed file which takes the machine name as `$1` and prints out a list of the commands that it adds to the launcher, this is run when the user invokes the `simh <machine> help` command.

## Releasing
To release a new version of the launcher you must tag the head of the `master` branch with the version number and the `latest` tag. The following commands will do this;

```bash
git checkout master
git fetch
git pull

# Tag the head with the version number
git tag -a 1.04

# Remove the existing latest tag and add it to the head
git push origin :refs/tags/latest
git tag -fa latest

# Push the tags to origin
git push origin master --tags
```

## License
The license for this source code is contained in the `LICENSE` file. TL;DR: It's an MIT license, do whatever you want with it.
